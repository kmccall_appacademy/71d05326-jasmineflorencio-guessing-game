# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def guessing_game
  special_number = rand(1..100)
  puts "guess a number"
  guess = gets.chomp.to_i
  counter = 1
  until special_number == guess
    if guess > special_number
    puts "Nah, #{guess} is too high. Try again!"
    guess = gets.chomp.to_i
    counter += 1
    elsif guess < special_number
      puts "Nope, #{guess} is too low. Try again!"
      guess = gets.chomp.to_i
      counter += 1
    end
  end

  if counter == 1
    puts "Lucky! It only took you #{counter} guess to figure out that special
    number is #{guess.to_s}."
  else
    puts "Yay! You got it! And it only took you #{counter} guesses to figure
    out that special number is #{guess.to_s}."
  end
end
